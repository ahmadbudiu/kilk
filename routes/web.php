<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function() {
    Route::resource('teacher', 'TeacherController', ['except' => ['show']]);
    Route::resource('student', 'StudentController', ['except' => ['show']]);
    Route::resource('class', 'ClassController');
    Route::get('/class/pdf/download', 'ClassController@download')->name('class.download');
    Route::post('/class/pdf/download', 'ClassController@getPDF')->name('class.getPDF');
});

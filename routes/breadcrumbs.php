<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Teacher
Breadcrumbs::for('teacher.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Teacher', route('teacher.index'));
});
Breadcrumbs::for('teacher.create', function ($trail) {
    $trail->parent('teacher.index');
    $trail->push('Create', route('teacher.create'));
});
Breadcrumbs::for('teacher.edit', function ($trail, $teacher) {
    $trail->parent('teacher.index');
    $trail->push($teacher->id);
    $trail->push('Edit', route('teacher.edit', $teacher->id));
});

// Class
Breadcrumbs::for('class.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Class', route('class.index'));
});
Breadcrumbs::for('class.create', function ($trail) {
    $trail->parent('class.index');
    $trail->push('Create', route('class.create'));
});
Breadcrumbs::for('class.show', function ($trail, $class_id) {
    $trail->parent('class.index');
    $trail->push($class_id, route('class.show', $class_id));
});
Breadcrumbs::for('class.edit', function ($trail, $class) {
    $trail->parent('class.show', $class->id);
    $trail->push('Edit', route('class.edit', $class->id));
});
Breadcrumbs::for('class.download', function ($trail) {
    $trail->parent('class.index');
    $trail->push('Download', route('class.download'));
});

// Student
Breadcrumbs::for('student.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Student', route('student.index'));
});
Breadcrumbs::for('student.create', function ($trail) {
    $trail->parent('student.index');
    $trail->push('Create', route('student.create'));
});
Breadcrumbs::for('student.edit', function ($trail, $student) {
    $trail->parent('student.index');
    $trail->push($student->id);
    $trail->push('Edit', route('student.edit', $student->id));
});

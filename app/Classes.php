<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';

    public function teacher() {
        return $this->belongsTo('App\Teacher');
    }

    public function students() {
        return $this->hasMany('App\Student', 'class_id');
    }
}

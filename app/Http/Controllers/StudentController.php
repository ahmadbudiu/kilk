<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Exception, Validator;
use App\Classes;
use App\Student;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        $students = Student::paginate(10);
        $data = [
            'students' => $students
        ];
        return view('student.index', $data);
    }

    public function create()
    {
        $classes = Classes::all();
        $data = [
            'classes' => $classes
        ];
        return view('student.create', $data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:50',
                'class_id' => 'required'
            ]);
            if($validator->fails()) {
                return redirect('student/create')->withErrors($validator)->withInput();
            }

            $student = new Student();
            $student->name = $request->name;
            $student->class_id = $request->class_id;
            $student->save();
            DB::commit();

            return redirect()->route('student.index')->with('flash_data', 'Data successfully created!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $student = Student::find($id);
        if(empty($student)) {
            return view('errors.404');
        }
        $classes = Classes::all();
        $data = [
            'student' => $student,
            'classes' => $classes
        ];
        return view('student.edit', $data);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            if($request->url) {
                $validator = Validator::make($request->all(), [
                    'class_id' => 'required'
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                    'name' => 'required|min:3|max:50',
                    'class_id' => 'required'
                ]);
            }
            if($validator->fails()) {
                return redirect()->route('student.edit', $id)->withErrors($validator)->withInput();
            }

            $student = Student::find($id);
            if(empty($student)) {
                return view('errors.500');
            }
            if(!$request->url) {
                $student->name = $request->name;
            }
            $student->class_id = $request->class_id;
            $student->save();
            DB::commit();

            if(!$request->url) {
                return redirect()->route('student.index')->with('flash_data', 'Data successfully updated!');
            } else {
                return redirect()->route('class.show', $request->class_id)
                                 ->with('flash_data', 'Data successfully updated!');
            }
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $student = Student::find($id);
            if(empty($student)) {
                return view('errors.500');
            }
            $student->delete();
            DB::commit();

            return redirect()->route('student.index')->with('flash_data', 'Data successfully deleted!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }
}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Exception, PDF, Validator;
use App\Classes;
use App\Student;
use App\Teacher;

class ClassController extends Controller
{
    public function index(Request $request)
    {
        $classes = Classes::paginate(10);
        $data = [
            'classes' => $classes
        ];
        return view('class.index', $data);
    }

    public function create()
    {
        $teachers = Teacher::all();
        $data = [
            'teachers' => $teachers
        ];
        return view('class.create', $data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:20',
                'teacher_id' => 'required'
            ]);
            if($validator->fails()) {
                return redirect('class/create')->withErrors($validator)->withInput();
            }

            $class = new Classes();
            $class->name = $request->name;
            $class->teacher_id = $request->teacher_id;
            $class->save();
            DB::commit();

            return redirect()->route('class.index')->with('flash_data', 'Data successfully created!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function show($id)
    {
        $class = Classes::find($id);
        if(empty($class)) {
            return view('errors.500');
        }
        $students = Student::where("class_id", "!=", $class->id)->get();
        $data = [
            'class' => $class,
            'students' => $students
        ];
        return view('class.show', $data);
    }

    public function edit($id)
    {
        $class = Classes::find($id);
        if(empty($class)) {
            return view('errors.404');
        }
        $teachers = Teacher::all();
        $data = [
            'class' => $class,
            'teachers' => $teachers
        ];
        return view('class.edit', $data);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:20',
                'teacher_id' => 'required'
            ]);
            if($validator->fails()) {
                return redirect()->route('class.edit', $id)->withErrors($validator)->withInput();
            }

            $class = Classes::find($id);
            if(empty($class)) {
                return view('errors.500');
            }
            $class->name = $request->name;
            $class->teacher_id = $request->teacher_id;
            $class->save();
            DB::commit();

            return redirect()->route('class.index')->with('flash_data', 'Data successfully updated!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $class = Classes::find($id);
            if(empty($class)) {
                return view('errors.500');
            }
            $class->delete();
            DB::commit();

            return redirect()->route('class.index')->with('flash_data', 'Data successfully deleted!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function download()
    {
        return view('class.download');
    }

    public function getPDF(Request $request)
    {
        try {
            if($request->id) {
                $classes = Classes::where('id', $request->id)->get();
            } else {
                $classes = Classes::all();
            }
            if(empty($classes)) {
                return view('errors.500');
            }
            $data = [
                'classes' => $classes
            ];
            $pdf = PDF::loadView('class.pdf_template', $data);
            return $pdf->stream();
        } catch(Exception $e) {
            return view('errors.500');
        }
    }
}

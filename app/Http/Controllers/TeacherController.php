<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Exception, Validator;
use App\Teacher;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
        $teachers = Teacher::paginate(10);
        $data = [
            'teachers' => $teachers
        ];
        return view('teacher.index', $data);
    }

    public function create()
    {
        return view('teacher.create');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:50'
            ]);
            if($validator->fails()) {
                return redirect('teacher/create')->withErrors($validator)->withInput();
            }

            $teacher = new Teacher();
            $teacher->name = $request->name;
            $teacher->save();
            DB::commit();

            return redirect()->route('teacher.index')->with('flash_data', 'Data successfully created!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $teacher = Teacher::find($id);
        if(empty($teacher)) {
            return view('errors.404');
        }
        $data = [
            'teacher' => $teacher
        ];
        return view('teacher.edit', $data);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:50'
            ]);
            if($validator->fails()) {
                return redirect()->route('teacher.edit', $id)->withErrors($validator)->withInput();
            }

            $teacher = Teacher::find($id);
            if(empty($teacher)) {
                return view('errors.500');
            }
            $teacher->name = $request->name;
            $teacher->save();
            DB::commit();

            return redirect()->route('teacher.index')->with('flash_data', 'Data successfully updated!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }

    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $teacher = Teacher::find($id);
            if(empty($teacher)) {
                return view('errors.500');
            }
            $teacher->delete();
            DB::commit();

            return redirect()->route('teacher.index')->with('flash_data', 'Data successfully deleted!');
        } catch(Exception $e) {
            DB::rollback();
            return view('errors.500');
        }
    }
}

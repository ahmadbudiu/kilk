@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{ Breadcrumbs::render('student.index') }}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Student</div>

                <div class="card-body">
                    @if (session('flash_data'))
                        <div class="alert alert-success">
                            {{ session('flash_data') }}
                        </div>
                    @endif
                    <a href="{{ route('student.create') }}">
                        <button class="btn btn-primary">Add New</button><br/><br/>
                    </a>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($students as $key => $student)
                                <tr>
                                    <td>{{ $key+$students->firstItem() }}</td>
                                    <td>{{ $student->name }}</td>
                                    <td>{{ $student->classes->name }}</td>
                                    <td>
                                        <a href="{{ route('student.edit', $student->id) }}">
                                            <button type="button" class="btn btn-info" >Edit</button>
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                            onclick="$('#delete-form-{{ $student->id }}').submit();">
                                            Delete
                                        </button>
                                        <form id="delete-form-{{ $student->id }}"
                                            action="{{ route('student.destroy', $student->id) }}"
                                            method="post" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p>Page {{ $students->currentPage() }} of {{ $students->lastPage() }}</p>
                    <a href="{{ $students->previousPageUrl() }}">
                        <button type="button" class="btn btn-default"
                            @if($students->currentPage() == 1)
                                disabled
                            @endif >
                            <<
                        </button>
                    </a>
                    <a href="{{ $students->nextPageUrl() }}">
                        <button type="button" class="btn btn-default"
                            @if($students->currentPage() == $students->lastPage())
                                disabled
                            @endif >
                            >>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

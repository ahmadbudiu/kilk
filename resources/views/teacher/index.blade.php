@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{ Breadcrumbs::render('teacher.index') }}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Teacher</div>

                <div class="card-body">
                    @if (session('flash_data'))
                        <div class="alert alert-success">
                            {{ session('flash_data') }}
                        </div>
                    @endif
                    <a href="{{ route('teacher.create') }}">
                        <button class="btn btn-primary">Add New</button><br/><br/>
                    </a>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($teachers as $key => $teacher)
                                <tr>
                                    <td>{{ $key+$teachers->firstItem() }}</td>
                                    <td>{{ $teacher->name }}</td>
                                    <td>
                                        <a href="{{ route('teacher.edit', $teacher->id) }}">
                                            <button type="button" class="btn btn-info" >Edit</button>
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                            onclick="$('#delete-form-{{ $teacher->id }}').submit();">
                                            Delete
                                        </button>
                                        <form id="delete-form-{{ $teacher->id }}"
                                            action="{{ route('teacher.destroy', $teacher->id) }}"
                                            method="post" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p>Page {{ $teachers->currentPage() }} of {{ $teachers->lastPage() }}</p>
                    <a href="{{ $teachers->previousPageUrl() }}">
                        <button type="button" class="btn btn-default"
                            @if($teachers->currentPage() == 1)
                                disabled
                            @endif>
                            <<
                        </button>
                    </a>
                    <a href="{{ $teachers->nextPageUrl() }}">
                        <button type="button" class="btn btn-default"
                            @if($teachers->currentPage() == $teachers->lastPage())
                                disabled
                            @endif>
                            >>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

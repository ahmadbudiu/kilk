@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{ Breadcrumbs::render('class.download') }}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Class</div>

                <div class="card-body">
                    <p>Klik button bellow to download file.</p>
                    <button type="button" class="btn btn-primary"
                        onclick="event.preventDefault();
                                    $('#download-form').submit();">
                        Download</button>
                    <form id="download-form" action="{{ route('class.getPDF') }}"
                        method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

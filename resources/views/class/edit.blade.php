@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{ Breadcrumbs::render('class.edit', $class) }}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Class</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            {{ $errors->all()[0] }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('class.update', $class->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $class->name }}">
                        </div>
                        <div class="form-group">
                            <label for="teacher_id">Teacher</label>
                            <select name="teacher_id" class="form-control">
                                @foreach($teachers as $teacher)
                                    <option value="{{ $teacher->id }}"
                                        @if($teacher->id == $class->teacher_id)
                                            selected
                                        @endif >
                                        {{ $teacher->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ route('class.index') }}">
                            <button type="button" class="btn btn-default">Cancel</button>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!DOCTYPE html>
<html>
<head>
    <title>Download PDF</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        body {
            background-color: #fff;
        }
    </style>
</head>
<body>
    <div class="container">
        @foreach($classes as $class)
            <div class="row">
                <div class="col-md-12">
                    <p>
                        Classroom: {{ $class->name }}<br>
                        Teacher: {{ $class->teacher->name }}
                    </p>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-12">
                    <span>Student list</span>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($class->students as $key => $student)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $student->name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    </div>
</body>
</html>
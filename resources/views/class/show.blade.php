@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{ Breadcrumbs::render('class.show', $class->id) }}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Class</div>

                <div class="card-body">
                    @if (session('flash_data'))
                        <div class="alert alert-success">
                            {{ session('flash_data') }}
                        </div>
                    @endif
                    <p>
                        Teacher: {{ $class->teacher->name }}
                        <a href="{{ route('class.edit', $class->id) }}">
                            <button type="button" class="btn btn-info">Change</button>
                        </a>
                    </p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Add New Student
                    </button><br/><br/>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Name</th>
                        </thead>
                        <tbody>
                            @foreach($class->students as $key => $student)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $student->name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="modal fade" id="exampleModal" tabindex="-1"
            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $key => $student)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $student->name }}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary"
                                                onclick="$('#add-form-{{ $student->id }}').submit();">
                                                Add
                                            </button>
                                            <form id="add-form-{{ $student->id }}"
                                                action="{{ route('student.update', $student->id) }}"
                                                method="post" style="display: none;">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <input type="hidden" name="url" value="class.show">
                                                <input type="hidden" name="class_id" value="{{ $class->id }}">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

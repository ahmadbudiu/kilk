@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{ Breadcrumbs::render('class.index') }}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Class</div>

                <div class="card-body">
                    @if (session('flash_data'))
                        <div class="alert alert-success">
                            {{ session('flash_data') }}
                        </div>
                    @endif
                    <a href="{{ route('class.create') }}">
                        <button class="btn btn-primary">Add New</button><br/><br/>
                    </a>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Name</th>
                            <th>Teacher</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($classes as $key => $class)
                                <tr>
                                    <td>{{ $key+$classes->firstItem() }}</td>
                                    <td>{{ $class->name }}</td>
                                    <td>{{ $class->teacher->name }}</td>
                                    <td>
                                        <a href="{{ route('class.show', $class->id) }}">
                                            <button type="button" class="btn btn-warning" >Show</button>
                                        </a>
                                        <a href="{{ route('class.edit', $class->id) }}">
                                            <button type="button" class="btn btn-info" >Edit</button>
                                        </a>
                                        <button type="button" class="btn btn-danger"
                                            onclick="$('#delete-form-{{ $class->id }}').submit();">
                                            Delete
                                        </button>
                                        <form id="delete-form-{{ $class->id }}"
                                            action="{{ route('class.destroy', $class->id) }}"
                                            method="post" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                        <button type="button" class="btn btn-primary"
                                            onclick="event.preventDefault();
                                                        $('#download-form-{{ $class->id }}').submit();">
                                            Download</button>
                                        <form id="download-form-{{ $class->id }}"
                                            action="{{ route('class.getPDF') }}"
                                            method="POST" style="display: none;">
                                            <input type="hidden" name="id" value="{{ $class->id }}">
                                            {{ csrf_field() }}
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p>Page {{ $classes->currentPage() }} of {{ $classes->lastPage() }}</p>
                    <a href="{{ $classes->previousPageUrl() }}">
                        <button type="button" class="btn btn-default"
                            @if($classes->currentPage() == 1)
                                disabled
                            @endif >
                            <<
                        </button>
                    </a>
                    <a href="{{ $classes->nextPageUrl() }}">
                        <button type="button" class="btn btn-default"
                            @if($classes->currentPage() == $classes->lastPage())
                                disabled
                            @endif>
                            >>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

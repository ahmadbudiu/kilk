<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the users table
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $json = File::get(database_path('json/users.json'));
        $data = json_decode($json);
        $data = collect($data);
        foreach ($data as $d) {
            $user = new User();
            $user->id = $d->id;
            $user->name = $d->name;
            $user->email = $d->email;
            $user->password = bcrypt($d->password);
            $user->remember_token = $d->remember_token;
            $user->save();
        }
    }
}

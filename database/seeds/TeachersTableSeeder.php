<?php

use Illuminate\Database\Seeder;
use App\Teacher;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(database_path('json/teachers.json'));
        $data = json_decode($json);
        $data = collect($data);
        foreach ($data as $d) {
            $teacher = new Teacher();
            $teacher->id = $d->id;
            $teacher->name = $d->name;
            $teacher->save();
        }
    }
}

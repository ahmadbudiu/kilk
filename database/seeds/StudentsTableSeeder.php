<?php

use Illuminate\Database\Seeder;
use App\Student;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(database_path('json/students.json'));
        $data = json_decode($json);
        $data = collect($data);
        foreach ($data as $d) {
            $student = new Student();
            $student->id = $d->id;
            $student->class_id = $d->class_id;
            $student->name = $d->name;
            $student->save();
        }
    }
}

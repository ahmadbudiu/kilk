<?php

use Illuminate\Database\Seeder;
use App\Classes;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(database_path('json/classes.json'));
        $data = json_decode($json);
        $data = collect($data);
        foreach ($data as $d) {
            $class = new Classes();
            $class->id = $d->id;
            $class->teacher_id = $d->teacher_id;
            $class->name = $d->name;
            $class->save();
        }
    }
}
